﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml.Serialization;

namespace AspirePaceclock
{
    public class ControlSetting
    {
        public const string _ControlListFilename = @"Resources\Controls.xml";

        public int ControlID { get; set; }
        public string ControlName { get; set; }
        public int ControlRow { get; set; }
        public int ControlCol { get; set; }
        public string FontColor { get; set; }
        public string FontFamilyName { get; set; }
        public string ImageFilePath { get; set; }
        public string ImageFolderPath { get; set; }
        public string ControlText { get; set; }
        public List<string> ImageFilenameList { get; set; }
        public int ImageScrollDurationMS { get; set; }
        public int ImageFilenameListPosition { get; set; }


        public ControlSetting()
        {
            ControlID = 0;
            ControlName = string.Empty;
            ControlRow = -1;
            ControlCol = -1;
            FontColor = "Yellow";
            FontFamilyName = "Segoe UI";
            ImageFilePath = string.Empty;
            ImageFolderPath = string.Empty;
            ImageScrollDurationMS = 5000;
            ImageFilenameListPosition = 0;
            ControlText = string.Empty;
            ImageFilenameList = new List<string>();
        }

        public static List<ControlSetting> ReadControlListXMLFile()
        {
            List<ControlSetting> controls = new List<ControlSetting>();

            if (File.Exists(_ControlListFilename))
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(List<ControlSetting>));
                using (var reader = new StreamReader(_ControlListFilename))
                {
                    controls = (List<ControlSetting>)deserializer.Deserialize(reader);
                }
            }

            AppUtilities.log.Info(string.Format("Successfully read {0} controls from {1}", controls.Count, _ControlListFilename));

            return controls;
        }

        public static void WriteControlListXMLFile(List<ControlSetting> controlSettings)
        {
            if (File.Exists(_ControlListFilename))
            {
                // Clear the file before writing it over
                FileStream f = new FileStream(_ControlListFilename, FileMode.Truncate);
                f.Close();
            }

            try
            {
                FileStream fs = new FileStream(_ControlListFilename, FileMode.OpenOrCreate);
                XmlSerializer serializer = new XmlSerializer(typeof(List<ControlSetting>));
                serializer.Serialize(fs, controlSettings);
                fs.Close();

                AppUtilities.log.Info(string.Format("Successfully wrote {0} controls to {1}", controlSettings.Count, _ControlListFilename));
            }
            catch (Exception ex)
            {
                AppUtilities.log.Error("Error writing controls data: " + ex.Message, ex);
                MessageBox.Show("Error saving controls data: " + ex.Message);
            }
        }

        public static List<ControlSetting> InitializeControlSettingsList()
        {
            List<ControlSetting> ControlSettings = new List<ControlSetting>();

            if (File.Exists(ControlSetting._ControlListFilename))
            {
                ControlSettings = ControlSetting.ReadControlListXMLFile();
            }
            else
            {
                for (int controlCounter = 1; controlCounter < 8; controlCounter++) // 
                {
                    ControlSetting cs = SetRowColData(controlCounter);
                    ControlSettings.Add(cs);
                }
            }

            return ControlSettings;
        }

        private static ControlSetting SetRowColData(int controlCounter)
        {
            ControlSetting cs = new ControlSetting();
            cs.ControlID = controlCounter;
            switch (controlCounter)
            {
                case 1:
                    cs.ControlName = "Control1";
                    cs.ControlRow = 0;
                    cs.ControlCol = 0;
                    break;
                case 2:
                    cs.ControlName = "Control2";
                    cs.ControlRow = 0;
                    cs.ControlCol = 1;
                    break;
                case 3:
                    cs.ControlName = "Control3";
                    cs.ControlRow = 0;
                    cs.ControlCol = 2;
                    break;
                case 4:
                    cs.ControlName = "Paceclock";
                    cs.ControlRow = 1;
                    cs.ControlCol = 0;
                    break;
                case 5:
                    cs.ControlName = "DateDay";
                    cs.ControlRow = 2;
                    cs.ControlCol = 0;
                    break;
                case 6:
                    cs.ControlName = "Control6";
                    cs.ControlRow = 2;
                    cs.ControlCol = 1;
                    break;
                case 7:
                    cs.ControlName = "TimeOfDay";
                    cs.ControlRow = 2;
                    cs.ControlCol = 2;
                    break;
                default:
                    break;
            }
            return cs;
        }

        public bool GetImagesInDirectory(out int NumberOfFoundImages)
        {
            bool success = false;
            NumberOfFoundImages = 0;

            List<string> validextentions = new List<string> { "bmp", "jpg", "gif", "png", "jpeg" };

            try
            {
                if (ImageFolderPath.Length > 0)
                {
                    DirectoryInfo d = new DirectoryInfo(ImageFolderPath);

                    // Linq query to get all fileinfo details in a directory
                    //List<FileInfo> myFiles = (from file in d.GetFiles("*.*", SearchOption.AllDirectories)
                    //                          where validextentions.Contains(file.Extension.Replace(".", "").ToLower())
                    //                          select new FileInfo(file.FullName)).ToList();

                    // Linq query to get all filenames of a certain type
                    ImageFilenameList = (from file in d.GetFiles("*.*", SearchOption.AllDirectories)
                                         where validextentions.Contains(file.Extension.Replace(".", "").ToLower())
                                         select file.FullName).ToList();

                    NumberOfFoundImages = ImageFilenameList.Count;
                    success = true;
                }
            }
            catch (Exception ex)
            {
                AppUtilities.log.Error("Error getting images from directory!", ex);
                success = false;
            }

            return success;
        }

    }
}
