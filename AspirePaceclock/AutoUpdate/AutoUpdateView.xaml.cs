﻿using System;
using System.Diagnostics;
using System.Windows;

namespace AspirePaceclock.AutoUpdate
{
    /// <summary>
    /// Interaction logic for AutoUpdateView.xaml
    /// </summary>
    public partial class AutoUpdateView : Window
    {
        private AutoUpdate _autoUpdate;

        public AutoUpdateView()
        {
            InitializeComponent();

            _autoUpdate = null;
        }

        public AutoUpdateView(AutoUpdate autoUpdate)
        {
            InitializeComponent();

            _autoUpdate = autoUpdate;
            tbLocalVersion.Text = "Local Version: " + _autoUpdate.LocalVersion;
            tbRemoteVersion.Text = "Remote Version: " + _autoUpdate.RemoteVersion;
        }

        private void btnReleaseNotes_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(_autoUpdate.JsonChangeLogURL);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error launching web browser!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                AppUtilities.log.Info("Error launching web browser!", ex);
            }
        }

        private void btnInstallUpdate_Click(object sender, RoutedEventArgs e)
        {
            _autoUpdate.DoUpdate();
        }

        private void btnSkipUpdate_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
