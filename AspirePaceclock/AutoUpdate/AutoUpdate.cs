﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net;
using System.Windows.Input;
using System.Windows;
using Newtonsoft.Json.Linq;

namespace AspirePaceclock.AutoUpdate
{
    public class AutoUpdate
    {
        private readonly string _autoUpdateUrl = "https://aspiresoftwaredevelopment.com/SoftwarePackages/autoupdate_aspirepaceclock.json";
        private string _jsonVersion;
        private string _jsonSoftwareURL;
        private string _localDownloadDirectory;
        private string _localDownloadFilename;
        private GetAssemblyInfo _assemblyInfo;

        public string JsonChangeLogURL { get; set; }
        public string LocalVersion
        {
            get
            {
                return _assemblyInfo.AssemblyVersion;
            }
        }

        public string RemoteVersion
        {
            get
            {
                return _jsonVersion;
            }
        }

        public AutoUpdate()
        {
            _jsonVersion = string.Empty;
            _jsonSoftwareURL = string.Empty;
            JsonChangeLogURL = string.Empty;
            _localDownloadDirectory = Environment.GetEnvironmentVariable("USERPROFILE") + @"\" + "Downloads";
            _localDownloadFilename = string.Empty;

            _assemblyInfo = new GetAssemblyInfo();
        }

        public bool UpdateAvailable()
        {
            bool retVal = false;


            if (GetUpdateDetails())
            {
                Version remoteVersion = new Version(_jsonVersion);
                Version localVersion = new Version(_assemblyInfo.AssemblyVersion);

                if (remoteVersion.CompareTo(localVersion) > 0)
                {
                    AppUtilities.log.InfoFormat("Local Assembly Version: {0} - Remote Assembly Version: {1} - Update Available", localVersion, remoteVersion);
                    retVal = true;
                }
                else
                {
                    AppUtilities.log.InfoFormat("Local Assembly Version: {0} - Remote Assembly Version: {1}", localVersion, remoteVersion);
                    retVal = false;
                }
            }
            else
            {
                AppUtilities.log.Info("AutoUpdate:UpdateAvailable - Error getting remote update details");
            }

            return retVal;
        }

        public void DoUpdate()
        {
            if (DownloadUpdate())
            {
                MessageBoxResult mbr = MessageBox.Show("Aspire Paceclock will now close and attempt to perform the update.  Are you sure you want to continue?",
                    "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (mbr == MessageBoxResult.Yes)
                {
                    InstallUpdate();
                }
            }
        }
        private bool GetUpdateDetails()
        {
            bool retVal = false;

            string jsonFile = GetJsonData();

            try
            {
                JObject data = JObject.Parse(jsonFile);

                if (data.ContainsKey("version"))
                {
                    _jsonVersion = data.GetValue("version").Value<string>();
                }

                if (data.ContainsKey("url"))
                {
                    _jsonSoftwareURL = data.GetValue("url").Value<string>();
                }

                if (data.ContainsKey("changelog"))
                {
                    JsonChangeLogURL = data.GetValue("changelog").Value<string>();
                }

                retVal = true;
            }
            catch (Exception ex)
            {
                AppUtilities.log.Error("AutoUpdate:GetUpdateDetails - Error getting update details: ", ex);
                retVal = false;
            }

            return retVal;
        }

        private string GetJsonData()
        {
            string content = string.Empty;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_autoUpdateUrl);
                request.Timeout = 5000;
                request.ReadWriteTimeout = 5000;
                var client = (HttpWebResponse)request.GetResponse();

                Stream stream = client.GetResponseStream();
                using (StreamReader reader = new StreamReader(stream))
                {
                    content = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                AppUtilities.log.Error("AutoUpdate:GetJsonData - Error getting json data: ", ex);
            }

            return content;
        }

        private bool DownloadUpdate()
        {
            bool success = false;
            Mouse.OverrideCursor = Cursors.Wait;

            try
            {
                if (_jsonSoftwareURL.Length > 0)
                {
                    _localDownloadFilename = _jsonSoftwareURL.Substring(_jsonSoftwareURL.LastIndexOf('/') + 1);

                    if (File.Exists(_localDownloadDirectory + @"\" + _localDownloadFilename))
                    {
                        File.Delete(_localDownloadDirectory + @"\" + _localDownloadFilename);
                    }

                    using (var client = new HttpClient())
                    {
                        using (var s = client.GetStreamAsync(_jsonSoftwareURL))
                        {
                            using (var fs = new FileStream(_localDownloadDirectory + @"\" + _localDownloadFilename, FileMode.OpenOrCreate))
                            {
                                s.Result.CopyTo(fs);
                            }
                        }
                    }
                    AppUtilities.log.Info("AutoUpdate:DownloadUpdate - Update File Downloaded: " + _localDownloadDirectory + @"\" + _localDownloadFilename);
                    success = true;
                }
                else
                {
                    AppUtilities.log.Info("AutoUpdate:DownloadUpdate - URL not set!");
                    success = false;
                }
            }
            catch (Exception ex)
            {
                AppUtilities.log.Error("AutoUpdate:DownloadUpdate - Error downloading update!", ex);
            }

            Mouse.OverrideCursor = Cursors.Arrow;
            return success;
        }

        private void InstallUpdate()
        {
            if (File.Exists(_localDownloadDirectory + @"\" + _localDownloadFilename))
            {
                Process p = null;

                try
                {
                    AppUtilities.log.Info("AutoUpdate:InstallUpdate - launching MSI package: " + _localDownloadDirectory + @"\" + _localDownloadFilename);
                    p = Process.Start(_localDownloadDirectory + @"\" + _localDownloadFilename);
                }
                catch (Exception ex)
                {
                    AppUtilities.log.Error("AutoUpdate:InstallUpdate - Error launching process!", ex);
                }

                if (p != null)
                {
                    AppUtilities.log.Info("AutoUpdate:InstallUpdate - Install process started - Aspire Paceclock Closing");
                    Application.Current.Shutdown();
                }
            }
        }
    }
}
