﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AspirePaceclock
{
    public class GetAssemblyInfo
    {
        // The assembly information values.
        public string Title = string.Empty;
        public string Description = string.Empty;
        public string Company = string.Empty;
        public string Product = string.Empty;
        public string Copyright = string.Empty;
        public string Trademark = string.Empty;
        public string AssemblyVersion = string.Empty;
        public string FileVersion = string.Empty;
        //public string Guid = string.Empty;
        //public string NeutralLanguage = string.Empty;
        //public bool IsComVisible = false;

        public GetAssemblyInfo()
            : this(Assembly.GetExecutingAssembly())
        {

        }

        public GetAssemblyInfo(Assembly assembly)
        {
            // Get values from the assembly.
            AssemblyTitleAttribute titleAttr =
                GetAssemblyAttribute<AssemblyTitleAttribute>(assembly);
            if (titleAttr != null) Title = titleAttr.Title;

            AssemblyDescriptionAttribute assemblyAttr =
                GetAssemblyAttribute<AssemblyDescriptionAttribute>(assembly);
            if (assemblyAttr != null) Description =
                assemblyAttr.Description;

            AssemblyCompanyAttribute companyAttr =
                GetAssemblyAttribute<AssemblyCompanyAttribute>(assembly);
            if (companyAttr != null) Company = companyAttr.Company;

            AssemblyProductAttribute productAttr =
                GetAssemblyAttribute<AssemblyProductAttribute>(assembly);
            if (productAttr != null) Product = productAttr.Product;

            AssemblyCopyrightAttribute copyrightAttr =
                GetAssemblyAttribute<AssemblyCopyrightAttribute>(assembly);
            if (copyrightAttr != null) Copyright = copyrightAttr.Copyright;

            AssemblyTrademarkAttribute trademarkAttr =
                GetAssemblyAttribute<AssemblyTrademarkAttribute>(assembly);
            if (trademarkAttr != null) Trademark = trademarkAttr.Trademark;

            AssemblyVersion = assembly.GetName().Version.ToString();

            AssemblyFileVersionAttribute fileVersionAttr =
                GetAssemblyAttribute<AssemblyFileVersionAttribute>(assembly);
            if (fileVersionAttr != null) FileVersion =
                fileVersionAttr.Version;

            //GuidAttribute guidAttr = GetAssemblyAttribute<GuidAttribute>(assembly);
            //if (guidAttr != null) Guid = guidAttr.Value;

            //NeutralResourcesLanguageAttribute languageAttr =
            //    GetAssemblyAttribute<NeutralResourcesLanguageAttribute>(assembly);
            //if (languageAttr != null) NeutralLanguage =
            //    languageAttr.CultureName;

            //ComVisibleAttribute comAttr =
            //    GetAssemblyAttribute<ComVisibleAttribute>(assembly);
            //if (comAttr != null) IsComVisible = comAttr.Value;
        }

        // Return a particular assembly attribute value.
        public static T GetAssemblyAttribute<T>(Assembly assembly)
            where T : Attribute
        {
            // Get attributes of this type.
            object[] attributes =
                assembly.GetCustomAttributes(typeof(T), true);

            // If we didn't get anything, return null.
            if ((attributes == null) || (attributes.Length == 0))
                return null;

            // Convert the first attribute value into
            // the desired type and return it.
            return (T)attributes[0];
        }
    }
}
