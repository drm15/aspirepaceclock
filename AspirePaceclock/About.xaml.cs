﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AspirePaceclock
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : Window
    {
        private GetAssemblyInfo assemblyInfo;

        public About()
        {
            InitializeComponent();

            assemblyInfo = new GetAssemblyInfo();
            lblTitle.Content = "Title: " + assemblyInfo.Title;
            lblCompany.Content = assemblyInfo.Company;
            lblCopyright.Content = assemblyInfo.Copyright;
            lblVersion.Content = "Version: " + assemblyInfo.FileVersion;
            lblDescription.Content = assemblyInfo.Description;
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Hyperlink source = sender as Hyperlink;
            Process.Start(source.NavigateUri.ToString());
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void lblLicenseAgreement_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var appPath = System.AppDomain.CurrentDomain.BaseDirectory;
            Process.Start(appPath + @"Resources\SoftwareLicenseAgreement.pdf");
        }
    }
}
