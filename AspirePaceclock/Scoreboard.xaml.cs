﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace AspirePaceclock
{
    /// <summary>
    /// Interaction logic for Scoreboard.xaml
    /// </summary>
    public partial class Scoreboard : Window
    {
        public enum TimerState
        {
            StopWatch,
            TimeOfDay,
            CountdownTimer
        }

        private Stopwatch stopWatch;
        private DispatcherTimer _dispatcherTimer = new DispatcherTimer();
        private TimeSpan _timeSpan = new TimeSpan(); // For the countdown timer
        private string currentTime = string.Empty;
        private const string dateFormat = "M/d/yyyy";
        private const string timeFormat = "h:mm tt";
        private const string timeFormatLong = "h:mm:ss";
        private const string dayOfWeekFormat = "dddd";
        private List<ControlSetting> _controlSettings;
        private TimerState _timerState;

        public Scoreboard()
        {
            InitializeComponent();
            stopWatch = new Stopwatch();
            _dispatcherTimer.Tick += new EventHandler(dt_Tick);
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);

            tbDate.Text = DateTime.Now.Date.ToString(dateFormat);
            tbTimeOfDay.Text = DateTime.Now.ToString(timeFormat);

            _controlSettings = ControlSetting.InitializeControlSettingsList();
            InitializeControls();

            _timerState = TimerState.StopWatch;

            _dispatcherTimer.Start();

            AppUtilities.log.Info("Scoreboard Window Initialized");
        }

        public Scoreboard(List<ControlSetting> controlSettings)
        {
            InitializeComponent();
            stopWatch = new Stopwatch();
            _dispatcherTimer.Tick += new EventHandler(dt_Tick);
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);

            tbDate.Text = DateTime.Now.Date.ToString(dateFormat);
            tbTimeOfDay.Text = DateTime.Now.ToString(timeFormat);

            _controlSettings = controlSettings;
            InitializeControls();

            _timerState = TimerState.StopWatch;

            _dispatcherTimer.Start();

            AppUtilities.log.Info("Scoreboard Window Initialized");
        }

        public void SetTimerState(TimerState timerState)
        {
            AppUtilities.log.Info("Timer State: " + timerState.ToString());
            _timerState = timerState;
        }

        private void InitializeControls()
        {
            for (int controlCounter = 0; controlCounter < _controlSettings.Count; controlCounter++)
            {
                ControlSetting obj = _controlSettings[controlCounter];
                ControlObject controlObject = new ControlObject(obj);
                switch (controlCounter)
                {
                    // Set all of the control properties
                    case 0: // Control 1
                        Grid.SetRow(controlObject, 0);
                        Grid.SetColumn(controlObject, 0);
                        gridScoreboard.Children.Add(controlObject);
                        break;
                    case 1: // Control 2
                        Grid.SetRow(controlObject, 0);
                        Grid.SetColumn(controlObject, 1);
                        gridScoreboard.Children.Add(controlObject);
                        break;
                    case 2: // Control 3
                        Grid.SetRow(controlObject, 0);
                        Grid.SetColumn(controlObject, 2);
                        gridScoreboard.Children.Add(controlObject);
                        break;
                    case 3: // Control 4
                        SetControlProperties(obj, tbTimer);
                        break;
                    case 4: // Control 5
                        SetControlProperties(obj, tbDate);
                        SetControlProperties(obj, tbDay);
                        break;
                    case 5: // Control 6
                        Grid.SetRow(controlObject, 2);
                        Grid.SetColumn(controlObject, 1);
                        gridScoreboard.Children.Add(controlObject);
                        break;
                    case 6: // Control 7
                        SetControlProperties(obj, tbTimeOfDay);
                        break;
                    default:
                        break;
                }
            }
        }

        private void SetControlProperties(ControlSetting controlProperties, TextBlock control)
        {
            // This function is only used for text controls (center, lower-left, lower-right)
            control.FontFamily = new FontFamily(controlProperties.FontFamilyName);
            control.Foreground = (SolidColorBrush)new BrushConverter().ConvertFromString(controlProperties.FontColor);
            control.Visibility = Visibility.Visible;
        }

        void dt_Tick(object sender, EventArgs e)
        {
            switch (_timerState)
            {
                case TimerState.StopWatch:
                    UpdateStopwatch();
                    break;
                case TimerState.TimeOfDay:
                    tbTimer.Text = DateTime.Now.ToString(timeFormatLong);
                    break;
                case TimerState.CountdownTimer:
                    UpdateCountdownTimer();
                    break;
                default:
                    break;
            }

            // Always update the current time and text
            tbDate.Text = DateTime.Now.Date.ToString(dateFormat);
            tbTimeOfDay.Text = DateTime.Now.ToString(timeFormat);
            tbDay.Text = DateTime.Now.Date.ToString(dayOfWeekFormat);
        }

        private void UpdateCountdownTimer()
        {
            if (_timeSpan >= TimeSpan.Zero)
            {
                _timeSpan = _timeSpan.Add(TimeSpan.FromMilliseconds(-1000)); // FromSecond(-1) loses 2 seconds every 5 minutes - this will correct that
                if (_timeSpan < TimeSpan.Zero)
                {
                    _timeSpan = TimeSpan.Zero;
                }
                currentTime = String.Format("{0:00}:{1:00}:{2:00}", _timeSpan.Hours, _timeSpan.Minutes, _timeSpan.Seconds);
                tbTimer.Text = currentTime;
            }
            else
            {
                // Don't show a negative number
                _timeSpan = TimeSpan.Zero;
                currentTime = String.Format("{0:00}:{1:00}:{2:00}", _timeSpan.Hours, _timeSpan.Minutes, _timeSpan.Seconds);
                tbTimer.Text = currentTime;
            }
        }

        private void UpdateStopwatch()
        {
            if (stopWatch.IsRunning)
            {
                TimeSpan ts = stopWatch.Elapsed;
                currentTime = String.Format("{0:00}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds);
                tbTimer.Text = currentTime;
            }
        }

        public void SetCountdownTimer(double minutes, double seconds)
        {
            _timerState = TimerState.CountdownTimer;
            _dispatcherTimer.Interval = TimeSpan.FromSeconds(1);

            _timeSpan = TimeSpan.FromMinutes(minutes) + TimeSpan.FromSeconds(seconds);
        }

        public void StartStopwatch()
        {
            stopWatch.Start();
        }

        public void StopStopwatch()
        {
            if (stopWatch.IsRunning)
            {
                stopWatch.Stop();
            }
        }

        public void ResetStopwatch()
        {
            stopWatch.Reset();
            tbTimer.Text = "00:00:00";
        }

        public void UpdateControl(ControlSetting obj, string controlName)
        {
            ControlObject controlObject = new ControlObject(obj);

            switch (controlName)
            {
                case "btnRow1Col0":
                    RemoveControl(obj);
                    Grid.SetRow(controlObject, 0);
                    Grid.SetColumn(controlObject, 0);
                    gridScoreboard.Children.Add(controlObject);
                    break;
                case "btnRow1Col1":
                    RemoveControl(obj);
                    Grid.SetRow(controlObject, 0);
                    Grid.SetColumn(controlObject, 1);
                    gridScoreboard.Children.Add(controlObject);
                    break;
                case "btnRow1Col2":
                    RemoveControl(obj);
                    Grid.SetRow(controlObject, 0);
                    Grid.SetColumn(controlObject, 2);
                    gridScoreboard.Children.Add(controlObject);
                    break;
                case "btnRow2Col0":
                    SetControlProperties(obj, tbTimer);
                    break;
                case "btnRow3Col0":
                    SetControlProperties(obj, tbDate);
                    SetControlProperties(obj, tbDay);
                    break;
                case "btnRow3Col1":
                    RemoveControl(obj);
                    Grid.SetRow(controlObject, 2);
                    Grid.SetColumn(controlObject, 1);
                    gridScoreboard.Children.Add(controlObject);
                    break;
                case "btnRow3Col2":
                    SetControlProperties(obj, tbTimeOfDay);
                    break;
                default:
                    break;
            }
        }

        private void RemoveControl(ControlSetting controlProperties)
        {
            // Not the most efficient, but there aren't many controls to deal with so this works and is readable
            UIElement control = null;
            try
            {
                foreach (UIElement element in gridScoreboard.Children)
                {
                    if (element is UserControl)
                    {
                        if ((element as UserControl).Name == controlProperties.ControlName)
                        {
                            control = element;
                        }
                    }
                };

                if (control != null)
                {
                    gridScoreboard.Children.Remove(control);
                }
            }
            catch (Exception ex)
            {
                AppUtilities.log.Error("Error removing control from grid!", ex);
            }
        }

        public void Gridlines(bool showGridlines)
        {
            if (showGridlines)
            {
                SolidColorBrush whiteBrush = new SolidColorBrush();
                whiteBrush.Color = Colors.White;

                gsRow1.Background = whiteBrush;
                gsRow2.Background = whiteBrush;
                gsCol1Top.Background = whiteBrush;
                gsCol2Top.Background = whiteBrush;
                gsCol1Bottom.Background = whiteBrush;
                gsCol2Bottom.Background = whiteBrush;

                gsRow1.Visibility = Visibility.Visible;
                gsRow2.Visibility = Visibility.Visible;
                gsCol1Top.Visibility = Visibility.Visible;
                gsCol2Top.Visibility = Visibility.Visible;
                gsCol1Bottom.Visibility = Visibility.Visible;
                gsCol2Bottom.Visibility = Visibility.Visible;
            }
            else
            {
                SolidColorBrush blackBrush = new SolidColorBrush();
                blackBrush.Color = Colors.Black;

                gsRow1.Background = blackBrush;
                gsRow2.Background = blackBrush;
                gsCol1Top.Background = blackBrush;
                gsCol2Top.Background = blackBrush;
                gsCol1Bottom.Background = blackBrush;
                gsCol2Bottom.Background = blackBrush;

                gsRow1.Visibility = Visibility.Hidden;
                gsRow2.Visibility = Visibility.Hidden;
                gsCol1Top.Visibility = Visibility.Hidden;
                gsCol2Top.Visibility = Visibility.Hidden;
                gsCol1Bottom.Visibility = Visibility.Hidden;
                gsCol2Bottom.Visibility = Visibility.Hidden;
            }
        }

        public double GetGridSize(string name)
        {
            double retVal = 0;

            switch (name)
            {
                case "gsRow1":
                    retVal = gsRow1.ActualWidth;
                    break;
                case "gsRow2":
                    retVal = gsRow2.ActualWidth;
                    break;
                case "gsCol1Top":
                    retVal = gsCol1Top.ActualHeight;
                    break;
                case "gsCol2Top":
                    retVal = gsCol2Top.ActualHeight;
                    break;
                case "gsCol1Bottom":
                    retVal = gsCol1Bottom.ActualHeight;
                    break;
                case "gsCol2Bottom":
                    retVal = gsCol2Bottom.ActualHeight;
                    break;
                default:
                    break;
            }

            return retVal;
        }
    }
}
