﻿using System.Windows;
using System;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Threading;

namespace AspirePaceclock
{
    /// <summary>
    /// Interaction logic for ControlObject.xaml
    /// </summary>
    public partial class ControlObject : UserControl
    {
        private ControlSetting controlSettings;
        private DispatcherTimer _imageScrollDispatchTimer = new DispatcherTimer();

        public ControlObject()
        {
            InitializeComponent();

            controlSettings = new ControlSetting();
        }

        public ControlObject(ControlSetting settings)
        {
            InitializeComponent();

            controlSettings = settings;
            SetControlSettings(controlSettings);
        }

        public void SetControlSettings(ControlSetting settings)
        {
            // Fixes a bug with the old version where the name of the control was just a number
            if (controlSettings.ControlName.Length < 7)
            {
                controlSettings.ControlName = "Conrol" + controlSettings.ControlName;
            }

            if (controlSettings.ControlName == "Date / Day")
            {
                controlSettings.ControlName = "DateDay";
            }

            if (controlSettings.ControlName == "Time of Day")
            {
                controlSettings.ControlName = "TimeOfDay";
            }

            controlSettings = settings;
            this.Name = controlSettings.ControlName;
            lblText.Content = controlSettings.ControlText;
            lblText.FontFamily = new FontFamily(controlSettings.FontFamilyName);
            lblText.Foreground = (SolidColorBrush)new BrushConverter().ConvertFromString(controlSettings.FontColor);

            if ((controlSettings.ImageFilePath != null ) &&
                (controlSettings.ImageFilePath.Length > 0))
            {
                UpdateControlWithImage(controlSettings.ImageFilePath);
            }

            if ((controlSettings.ImageFolderPath != null) &&
                (controlSettings.ImageFolderPath.Length > 0))
            {
                ScrollImage(); // First time before the timer ticks
                _imageScrollDispatchTimer.Tick += (s, args) => ScrollImage();
                _imageScrollDispatchTimer.Interval = new TimeSpan(0, 0, 0, 0, controlSettings.ImageScrollDurationMS);
                _imageScrollDispatchTimer.Start();
            }
        }

        private void UpdateControlWithImage(string filePath)
        {
            if (File.Exists(filePath))
            {
                Image image = CreateImageFromFile(filePath);
                controlGrid.Children.Clear();
                controlGrid.Children.Add(image);
            }
            else
            {
                lblText.Content = "Image Not Found";
            }
        }

        private static Image CreateImageFromFile(string filename)
        {
            Image scoreboardImage = new Image();

            try
            {
                ImageSource image = new BitmapImage(new Uri(filename, UriKind.RelativeOrAbsolute));
                Style style = new Style(typeof(DataGridColumnHeader));
                FrameworkElementFactory factory = new FrameworkElementFactory(typeof(Image));
                factory.SetValue(Image.SourceProperty, image);
                factory.SetValue(Image.StretchProperty, Stretch.Uniform);
                style.Setters.Add(new Setter { Property = TemplateProperty, Value = new ControlTemplate { TargetType = typeof(DataGridColumnHeader), VisualTree = factory } });
                scoreboardImage.Source = image;
            }
            catch (Exception ex)
            {
                AppUtilities.log.Error("Error creating image from file!", ex);
            }

            return scoreboardImage;
        }

        private void ScrollImage()
        {
            this.Dispatcher.Invoke(new Action(delegate ()
            {
                if (controlSettings.ImageFilenameList.Count > 0)
                {
                    if (controlSettings.ImageFilenameList.Count > controlSettings.ImageFilenameListPosition)
                    {
                        string filename = controlSettings.ImageFilenameList[controlSettings.ImageFilenameListPosition];
                        UpdateControlWithImage(filename);
                        controlSettings.ImageFilenameListPosition++;
                    }
                    else
                    {
                        controlSettings.ImageFilenameListPosition = 0; // We have reached the end of the list - start over at the beginning
                    }
                }
            }));
        }
    }
}
