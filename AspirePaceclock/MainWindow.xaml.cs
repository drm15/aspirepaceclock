﻿using log4net.Config;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows;

namespace AspirePaceclock
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MessageProcessor _processMessages;
        private List<ControlSetting> _controlSettings;

        public MainWindow()
        {
            InitializeComponent();
            XmlConfigurator.Configure();
            AppUtilities.log.Info("Aspire Paceclock Initialized");

            _controlSettings = ControlSetting.InitializeControlSettingsList();
            _processMessages = new MessageProcessor(_controlSettings);

            AppUtilities.AutoUpdateChecker(false);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            AppUtilities.log.Info("Aspire Paceclock Closing");
            ControlSetting.WriteControlListXMLFile(_controlSettings);

            _processMessages.CloseScoreboardWindow();

            Application.Current.Shutdown();
            Environment.Exit(0);
        }

        private void HelpMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItemFunctions.ShowHelp();
        }

        private void MenuItemCheckForUpdates_Click(object sender, RoutedEventArgs e)
        {
            MenuItemFunctions.CheckForUpdates();
        }

        private void ReleaseNotes_Click(object sender, RoutedEventArgs e)
        {
            MenuItemFunctions.ShowReleaseNotes();
        }

        private void HelpRemoteDesktopSupport_Click(object sender, RoutedEventArgs e)
        {
            MenuItemFunctions.RemoteDesktopSupportGoogle();
        }

        private void HelpRemoteDesktopWindows_Click(object sender, RoutedEventArgs e)
        {
            MenuItemFunctions.RemoteDesktopSupportMicrosoft();
        }

        private void AboutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItemFunctions.ShowAbout();
        }

        private void btnOpenPaceClock_Click(object sender, RoutedEventArgs e)
        {
            _processMessages.OpenScoreboardWindow();
        }

        private void OpenScoreboardWindow()
        {
            _processMessages.OpenScoreboardWindow();
        }

        private void btnStartPaceClock_Click(object sender, RoutedEventArgs e)
        {
            _processMessages.StartPaceclock();
        }

        private void btnStopPaceClock_Click(object sender, RoutedEventArgs e)
        {
            _processMessages.StopPaceclock();
        }

        private void btnResetPaceClock_Click(object sender, RoutedEventArgs e)
        {
            _processMessages.ResetPaceclock();
        }
        private void btnClosePaceClock_Click(object sender, RoutedEventArgs e)
        {
            _processMessages.ClosePaceclock();
        }

        private void btnShowTimeOfDay_Click(object sender, RoutedEventArgs e)
        {
            _processMessages.ShowTimeOfDay();
        }

        private void btnCountDownTimerStart_Click(object sender, RoutedEventArgs e)
        {
            double minutes = 0;
            double.TryParse(tbMinutes.Text, out minutes);
            double seconds = 0;
            double.TryParse(tbSeconds.Text, out seconds);
            _processMessages.StartCountdownTimer(minutes, seconds);
        }

        private void tbMinutes_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void tbSeconds_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void LoadControlSettingsForm(object sender, ControlSetting controlSetting)
        {
            ControlSettings frmControlSettings = new ControlSettings(controlSetting);
            frmControlSettings.Owner = this;
            frmControlSettings.ShowDialog();

            if (frmControlSettings.DialogResult.HasValue && frmControlSettings.DialogResult.Value)
            {
                string btnName = ((System.Windows.FrameworkElement)sender).Name;
                ControlSetting obj = frmControlSettings._Control;

                switch (btnName)
                {
                    case "btnRow1Col0":
                        _controlSettings[0] = obj;
                        break;
                    case "btnRow1Col1":
                        _controlSettings[1] = obj;
                        break;
                    case "btnRow1Col2":
                        _controlSettings[2] = obj;
                        break;
                    case "btnRow2Col0":
                        _controlSettings[3] = obj;
                        break;
                    case "btnRow3Col0":
                        _controlSettings[4] = obj;
                        break;
                    case "btnRow3Col1":
                        _controlSettings[5] = obj;
                        break;
                    case "btnRow3Col2":
                        _controlSettings[6] = obj;
                        break;
                    default:
                        break;
                }

                _processMessages.UpdateControl(obj, btnName);
            }
        }

        private void btnRow1Col0_Click(object sender, RoutedEventArgs e)
        {
            LoadControlSettingsForm(sender, _controlSettings[0]);
        }

        private void btnRow1Col1_Click(object sender, RoutedEventArgs e)
        {
            LoadControlSettingsForm(sender, _controlSettings[1]);
        }

        private void btnRow1Col2_Click(object sender, RoutedEventArgs e)
        {
            LoadControlSettingsForm(sender, _controlSettings[2]);
        }

        private void btnRow2Col0_Click(object sender, RoutedEventArgs e)
        {
            LoadControlSettingsForm(sender, _controlSettings[3]);
        }

        private void btnRow3Col0_Click(object sender, RoutedEventArgs e)
        {
            LoadControlSettingsForm(sender, _controlSettings[4]);
        }

        private void btnRow3Col1_Click(object sender, RoutedEventArgs e)
        {
            LoadControlSettingsForm(sender, _controlSettings[5]);
        }

        private void btnRow3Col2_Click(object sender, RoutedEventArgs e)
        {
            LoadControlSettingsForm(sender, _controlSettings[6]);
        }
        
        private void cbShowGridLines_Click(object sender, RoutedEventArgs e)
        {
            _processMessages.ShowGridlines((bool)cbShowGridLines.IsChecked);

            if (!(bool)cbShowGridLines.IsChecked)
            {
                // TODO: Save this as Last Used Settings
                // Get the height/width settings of what was changed
                double gsRow1 = _processMessages.GetGridSize("gsRow1");
                double gsRow2 = _processMessages.GetGridSize("gsRow2");
                double gsCol1Top = _processMessages.GetGridSize("gsCol1Top");
                double gsCol2Top = _processMessages.GetGridSize("gsCol2Top");
                double gsCol1Bottom = _processMessages.GetGridSize("gsCol1Bottom");
                double gsCol2Bottom = _processMessages.GetGridSize("gsCol2Bottom");
            }
        }
    }
}
