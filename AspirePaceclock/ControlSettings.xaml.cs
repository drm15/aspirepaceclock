﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using FontFamily = System.Drawing.FontFamily;
using Microsoft.Win32;
using Color = System.Windows.Media.Color;
using ColorConverter = System.Windows.Media.ColorConverter;
using FolderBrowserForWPF;

namespace AspirePaceclock
{
    /// <summary>
    /// Interaction logic for ControlSettings.xaml
    /// </summary>
    public partial class ControlSettings : Window
    {
        public ControlSetting _Control { get; set; }
        // TODO: Set control settings and bind to form values

        public ControlSettings()
        {
            InitializeComponent();

            LoadFontCombobox();
            LoadFontColorsComboBox();

            _Control = new ControlSetting();

            // Set Control values in the fields
            SetComboBoxFontColor();
            SetComboBoxFontName();
            SetDirectoryPathAndImageCount();
        }

        public ControlSettings(ControlSetting control)
        {
            InitializeComponent();

            LoadFontCombobox();
            LoadFontColorsComboBox();

            _Control = control;

            // Set Control values in the fields
            SetComboBoxFontColor();
            SetComboBoxFontName();
            lblCtrlName.Content = _Control.ControlID;
            tbText.Text = _Control.ControlText;
            SetImageButtonContent(_Control.ImageFilePath);
            SetDirectoryPathAndImageCount();
            iudScrollDuration.Value = (int)(_Control.ImageScrollDurationMS / 1000);

            switch (control.ControlID)
            {
                case 4: // Timer
                    DisableButtons();
                    break;
                case 5: // DateDay
                    DisableButtons();
                    break;
                case 7: // TimeOfDay
                    DisableButtons();
                    break;
                default:
                    EnableButtons();
                    break;
            }
        }

        private void EnableButtons()
        {
            btnImage.IsEnabled = true;
            btnClearImage.IsEnabled = true;
            tbText.IsEnabled = true;
            btnImageFolder.IsEnabled = true;
            btnClearImageFolder.IsEnabled = true;
            iudScrollDuration.IsEnabled = true;
        }

        private void DisableButtons()
        {
            btnImage.IsEnabled = false;
            btnClearImage.IsEnabled = false;
            tbText.IsEnabled = false;
            btnImageFolder.IsEnabled = false;
            btnClearImageFolder.IsEnabled = false;
            iudScrollDuration.IsEnabled = false;
        }

        private void SetComboBoxFontName()
        {
            cmbFonts.SelectedValue = _Control.FontFamilyName;
        }

        private void LoadFontCombobox()
        {
            List<FontFamily> fontFamilies = System.Drawing.FontFamily.Families.ToList();
            List<string> fontNames = new List<string>();

            foreach (FontFamily family in fontFamilies)
                fontNames.Add(family.Name);
            cmbFonts.ItemsSource = fontNames;
        }

        private void SetComboBoxFontColor()
        {
            object convertFromString = ColorConverter.ConvertFromString(_Control.FontColor);
            if (convertFromString != null)
            {
                SolidColorBrush brush =
                    new SolidColorBrush((Color)convertFromString);
                System.Drawing.Color c = ColorTranslator.FromHtml(brush.ToString());
                cmbColors.SelectedValue = c.Name;
            }
        }

        private void LoadFontColorsComboBox()
        {
            List<string> fontColors = new List<string>();

            Type colorsType = typeof(Colors);
            PropertyInfo[] colorsTypePropertyInfos =
                colorsType.GetProperties(BindingFlags.Public | BindingFlags.Static);

            foreach (PropertyInfo colorsTypePropertyInfo in colorsTypePropertyInfos)
                fontColors.Add(colorsTypePropertyInfo.Name);

            cmbColors.ItemsSource = fontColors;

        }

        private void btnImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            if (ofd.ShowDialog() == true)
            {
                _Control.ImageFilePath = ofd.FileName;
                SetImageButtonContent(ofd.FileName);
                //btnImage.Content = ofd.FileName.Substring(ofd.FileName.LastIndexOf(@"\") + 1);
            }
        }

        private void SetImageButtonContent(string fileName)
        {
            if ((fileName != null ) && (fileName.Length > 0))
            {
                btnImage.Content = fileName.Substring(fileName.LastIndexOf(@"\") + 1);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            _Control.FontFamilyName = cmbFonts.SelectedValue.ToString();
            _Control.FontColor = cmbColors.SelectedValue.ToString();
            _Control.ControlText = tbText.Text;
            this.DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void btnClearImage_Click(object sender, RoutedEventArgs e)
        {
            _Control.ImageFilePath = string.Empty;
            btnImage.Content = "Select Image";
        }

        private void btnImageFolder_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserForWPF.Dialog dialog = new FolderBrowserForWPF.Dialog();
            dialog.Title = "Select Image Folder";
            var result = dialog.ShowDialog();

            if (result == true)
            {
                _Control.ImageFolderPath = dialog.FileName;
                SetDirectoryPathAndImageCount();
            }
        }

        private void SetDirectoryPathAndImageCount()
        {
            int NumberOfImages;
            if (!_Control.GetImagesInDirectory(out NumberOfImages) && _Control.ImageFolderPath.Length != 0)
            {
                MessageBox.Show("Error finding images in folder!  See log for details", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            tbFolderPath.Text = string.Format("{0} ({1} images)", _Control.ImageFolderPath, NumberOfImages);
        }

        private void btnClearImageFolder_Click(object sender, RoutedEventArgs e)
        {
            _Control.ImageFolderPath = string.Empty;
            tbFolderPath.Text = string.Empty;
        }

        private void iudScrollDuration_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (iudScrollDuration.Value != null && Int32.TryParse(iudScrollDuration.Value.ToString(), out int scrollDuration))
            {
                _Control.ImageScrollDurationMS = scrollDuration * 1000;
            }
        }
    }
}
