﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspirePaceclock
{
    internal class MessageProcessor
    {
        private Scoreboard _ScoreboardWindow;
        private List<ControlSetting> _ControlSettings;

        public MessageProcessor(List<ControlSetting> controlSettings)
        {
            _ScoreboardWindow = null;
            _ControlSettings = controlSettings;
        }

        public void OpenScoreboardWindow()
        {
            AppUtilities.log.Info("Open Scoreboard Window");

            if (_ScoreboardWindow != null)
            {
                // Close the window if it is already open (don't create duplicate windows)
                _ScoreboardWindow.Close();
            }

            _ScoreboardWindow = new Scoreboard(_ControlSettings);
            _ScoreboardWindow.Show();
        }

        public void CloseScoreboardWindow()
        {
            AppUtilities.log.Info("Close Scoreboard Window");

            if (_ScoreboardWindow != null && _ScoreboardWindow.IsVisible)
            {
                _ScoreboardWindow.Close();
            }
        }

        public void ShowGridlines(bool showGridLines)
        {
            AppUtilities.log.InfoFormat("Show Gridlines: {0}", showGridLines);

            if (_ScoreboardWindow != null)
            {
                _ScoreboardWindow.Gridlines(showGridLines);
            }
        }

        public void StartPaceclock()
        {
            AppUtilities.log.Info("Start Pace Clock");

            if (_ScoreboardWindow == null)
            {
                OpenScoreboardWindow();
            }

            _ScoreboardWindow.SetTimerState(Scoreboard.TimerState.StopWatch);
            _ScoreboardWindow.StartStopwatch();
        }

        public void StopPaceclock()
        {
            AppUtilities.log.Info("Stop Pace Clock");

            if (_ScoreboardWindow != null)
            {
                _ScoreboardWindow.StopStopwatch();
            }
        }

        public void ResetPaceclock()
        {
            AppUtilities.log.Info("Reset Pace Clock");

            if (_ScoreboardWindow != null)
            {
                _ScoreboardWindow.SetTimerState(Scoreboard.TimerState.StopWatch);
                _ScoreboardWindow.ResetStopwatch();
            }
        }

        public void ClosePaceclock()
        {
            AppUtilities.log.Info("Close Pace Clock");

            if (_ScoreboardWindow != null)
            {
                _ScoreboardWindow.Close();
                _ScoreboardWindow = null;
            }
        }

        public void ShowTimeOfDay()
        {
            AppUtilities.log.Info("Show Time of Day");

            if (_ScoreboardWindow == null)
            {
                OpenScoreboardWindow();
            }

            _ScoreboardWindow.SetTimerState(Scoreboard.TimerState.TimeOfDay);
        }

        public void StartCountdownTimer(double minutes, double seconds)
        {
            AppUtilities.log.InfoFormat("Start Countdown Timer for {0} minutes and {1} seconds", minutes, seconds);

            if (_ScoreboardWindow == null)
            {
                OpenScoreboardWindow();
            }

            _ScoreboardWindow.SetTimerState(Scoreboard.TimerState.CountdownTimer);
            _ScoreboardWindow.ResetStopwatch(); // Reset the counter to zero
            _ScoreboardWindow.SetCountdownTimer(minutes, seconds);
        }

        public void UpdateControl(ControlSetting obj, string btnName)
        {
            AppUtilities.log.InfoFormat("Update Control on Scoreboard: {0}", btnName);

            if (_ScoreboardWindow != null)
            {
                _ScoreboardWindow.UpdateControl(obj, btnName);
            }
        }

        public double GetGridSize(string name)
        {
            double retVal = 0;
            
            if (_ScoreboardWindow != null)
            {
                retVal = _ScoreboardWindow.GetGridSize(name);
            }

            return retVal;
        }
    }
}
