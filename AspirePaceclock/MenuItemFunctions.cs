﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspirePaceclock
{
    public class MenuItemFunctions
    {
        public static void ShowHelp()
        {
            var appPath = System.AppDomain.CurrentDomain.BaseDirectory;
            Process.Start(appPath + @"Resources\AspirePaceclockHelpManual.pdf");
        }

        public static void CheckForUpdates()
        {
            AppUtilities.AutoUpdateChecker(true);
        }

        public static void ShowReleaseNotes()
        {
            try
            {
                Process.Start("https://aspiresoftwaredevelopment.com/SoftwarePackages/ReleaseNotes_Paceclock.html");
                AppUtilities.log.Info("Aspire Paceclock Release Notes Started");
            }
            catch (Exception ex)
            {
                AppUtilities.log.Error("Error starting Aspire Paceclock Release Notes", ex);
            }

        }

        public static void RemoteDesktopSupportGoogle()
        {
            // Google Remote Desktop
            try
            {
                Process.Start("https://remotedesktop.google.com/support");
                AppUtilities.log.Info("Google Remote Desktop Started");
            }
            catch (Exception ex)
            {
                AppUtilities.log.Error("Error starting Google Remote Desktop", ex);
            }
        }

        public static void RemoteDesktopSupportMicrosoft()
        {
            // Microsoft Remote Desktop
            Process process = new Process();

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            startInfo.FileName = "msra.exe";
            startInfo.Arguments = "/novice";

            try
            {
                process.StartInfo = startInfo;
                process.Start();
                AppUtilities.log.Info("Microsoft Remote Desktop Started");

            }
            catch (Exception ex)
            {
                AppUtilities.log.Error("Error starting Microsoft Remote Desktop", ex);
            }
        }

        public static void ShowAbout()
        {
            About dialog = new About();
            dialog.Show();
        }
    }
}
