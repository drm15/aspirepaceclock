﻿using System.Net.NetworkInformation;
using System.Windows;

namespace AspirePaceclock
{
    public class AppUtilities
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static bool IsValidInternetConnection()
        {
            bool retVal = false;

            retVal = NetworkInterface.GetIsNetworkAvailable();

            return retVal;
        }

        public static void AutoUpdateChecker(bool reportErrors)
        {
            if (AppUtilities.IsValidInternetConnection())
            {
                AppUtilities.log.Info("AutoUpdate: Valid internet connection - checking for updates");

                AutoUpdate.AutoUpdate autoUpdate = new AutoUpdate.AutoUpdate();
                if (autoUpdate.UpdateAvailable())
                {
                    AutoUpdate.AutoUpdateView autoUpdateView = new AutoUpdate.AutoUpdateView(autoUpdate);
                    autoUpdateView.ShowDialog();
                }
                else
                {
                    if (reportErrors)
                    {
                        MessageBox.Show("There is no update available", "No Update", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }

                AppUtilities.log.Info("AutoUpdate: Checking for updates complete");
            }
            else
            {
                if (reportErrors)
                {
                    // No internet connection
                    MessageBox.Show("There is no internet connection - please connect to the internet to check for updates.", "Error - Cannot Connect",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);

                    AppUtilities.log.Error("AutoUpdate: No internet connection");
                }
            }
        }
    }
}
